package net.mandaria.tippytipper.services;

import org.junit.Before;
import org.junit.Test;

import static junit.framework.Assert.assertEquals;

public class TipCalculatorServiceTest {

    private TipCalculatorService t;

    @Before
    public void setUp() {
        t = new TipCalculatorService();
        t.AppendNumberToBillAmount("100");
    }

    @Test
    public void shouldCalculateEmptyTip() {
        t.SetTipPercentage(0);
        t.CalculateTip();

        assertEquals("0,00 €", t.GetTipAmount());
    }

    @Test
    public void shouldCalculateEmptyTax() {
        t.SetTipPercentage(0);
        t.SetTaxPercentage(0);
        t.CalculateTip();

        assertEquals("0,00 €", t.GetTipAmount());
    }

    @Test
    public void shouldCalculatePlainBillAmount() {
        t.SetTipPercentage(0);
        t.SetTaxPercentage(0);
        t.CalculateTip();

        assertEquals("1,00 €", t.GetBillAmount());
    }

    @Test
    public void shouldCalculateTipAmount() {
        t.SetTipPercentage(0.5);
        t.SetTaxPercentage(0);
        t.CalculateTip();

        assertEquals("0,50 €", t.GetTipAmount());
    }
}